# Aetherium Core

An aetherium core is a magical device made of charged aetherium. They are used to power other constructs and mechanical devices. Aetherium Cores remain mostly stable but drawing too much power too fast can result in a catastrophic feedback event which will result in the core explosion. The power of the explosion is dependant on the amount of energy stored in the core.

An Aetherium Core can be recharged in magical energies either by draining a spellcaster energies into the crystal or by feeding it the energy contained into magical components. It is ill-advised to let a core deplete itself entirely of its energies as re-infusing it will require way more magical energies than simply recharging it.


>>>

#### Game mechanics

The Aetherium core provide powers to its associated systems. If removed or disrupted, these systems will cease to function. 

The Core has a number of structural hitpoints representing the stability and resistance of the core to damage. If the Core hitpoints fall to 0, it will suffer catastrophic failure and will cause a massive explosion in a radius of 100ft and will cause 250 points of raw magical damage to any creature caught in the blast.

Structural hit points : 10

AC : 22 

Any hit that go through the Core's AC will cause 1 point of structural damage. A critical success will cause 2. The Core can also be damaged by using subsystems or by external factors at the DM's discretion.


>>>