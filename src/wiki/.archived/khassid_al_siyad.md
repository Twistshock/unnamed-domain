# Master of the Hunt Khassid Al Siyad

Khassid Al Siyad is the Master of the Hunt in the Imperial outpost of Cliffport. He was granted authority over a team of witchers in charge of purging the [Shattered Coast](../locations/shattered_coast.md). His main objective is to make the lands around safe enough for future colonization efforts. 

## Description

Khassid is a half-elf. His skin is brown, tanned by the many years spent outside. His hairs are white and cut very short. His eyes are typical of witcher as they look like cat eyes. He has a neatly trimmed beard. 

His armor is made of leather taken from his many kills : Wyverns, manticores, scorpions... He adorns numerous scars on his body and from which he is greatly proud. On his armour he also wears prominently the symbol of his order and his current title. 

## Personality

Khassid is haughty and proud. He is convinced by his own greatness and few can hope to compete against his hunting skills. He sees the other member of his hunting party more as tools to be used in his hunts than real companions. He was in a relationship with huntress Sara but they broke up after fearing that being attached to someone would hinder his duties.

Fervent member of the Ordo Witcharius, he is strictly following the doctrines of the order and never questionning them as he believe they are what they require to fight the monsters. Any other school of thought not approved by the order must be eliminated to have a chance to fight off the monsters.

Although he is forced to collaborate with the Legion in securing the Shattered Coast, he sees them more as a distraction and often follow his own agenda.

## The Shattered Coast Hunting Team

To accomplish his mission of securing the Shattered Coast, Khassid was assigned a hunting team composed of the following members:

### Huntress Sara

Sara is a Tabaxi Female. She specializes in tracking and long range attacks. She is adept at sneaking and is renowned in the order for being able to infiltrate lairs of monsters without rousing them. 

### Protector Cuddles

Cuddles is an imposing Warforged. Larger than a Goliath, he wields his massive hammer with ruthless effectiveness. His armor and size make him slow but what he lacks in speed, he makes in stopping power. He is known in the order to have single-handedly stopped a Rhinox stampede by simply standing in their way and pushing them away.

### Youngblood Tariq

Tariq is a newly initiated hunter and still carries the title of Youngblood. He has yet to kill his first monster. He is inexperienced and lacks confidence. He looks up to the other hunters due to their fame and hopes to one day be as skilled as they are.

## Backstory

[TODO]


## Trivia

"Al Siyad" means "The Hunter" in arabic