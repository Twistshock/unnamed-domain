# The Vermillion Serpent

The Vermillion Serpent was the main object of devotion of the [Ophidian Sect](../organizations/ophidian_sect.md). It was usually represented as a massive , multi-headed red snake but could take the appearance of a woman of great beauty and clad in a long silky red robe. 

It first appeared to the High Priest Tessok Khaliss, first servant of the sun, in a time where the empire was on the brink of collapse. Best by demons and rebellious vassals, the Serpent seduced Tessok, convincing him that the Sun God [Teck'lict](teck_lict.md) abandonned his people. He boasted that he could replace the sun god if Tessok allowed him to swallow the [Blood of the Sun](../items/blood_of_the_sun.md), a powerful artifact that was kept in the royal temple. Building a cult and gaining influence, The Serpent managed to gain access to the artifact after his followers performed a successful coup against [Emperor Kuk'lu the burnt](kuklu.md).

The Serpent ingested the blood of the sun and in turn replaced it with a great Void that took form inside the temple. The sun went dark over the Empire and in a fortnight, great sandstorms swept the lands, reducing it all into a great desert.