# Celestial Spires

Built from a material black as the night, these massive tower run towards the sky with no apparent end. Many have tried to climb or observe the top of such structure but the only thing they were able to find were their own doom.

Their purpose are unknown and forgotten, only those with a deep understanding of the secrets of the universe itself remember why they exist.

## Known Spires and locations

- Spire of Truth (Capital Imperial City)
- Spire of Vigil (Wastes)
- Spire of the Gods (Brightlands)
- The unifinished Spire (Greymouth Bay)
- The Shattered Guardian (Blightlands) 
 
## Purpose

Each spire was built for te only purpose of housing a [Shard of creation](../items/shards-of-creation.md). The strange material and Celestial design of the spire act like an antenna to increase the stabilizing effect of the shard on reality, reinforcing the barrier between the planes and the void. It makes demonic incursions less likely to happen the closer to a spire you get.

## Defenses

### Force Field
Due to the great importance of these structures for the Celestials, they spared no effort making sure they would be the most protected places in all of the realms. Each spire is protected by an impenetrable force field surrounding the strucutre. Nothing can pass through it and spells cannot reach inside of it. The force field also provokes a shimmering effect making it difficult to see inside of the field.

The force field extends all the way up around the structure and all the way down. It is not possible to dig under it or fly above it.

### Deadly traps
In case something managed to get pass the force field, many traps lay hidden to tear apart any intruder. These traps range from simple mechanical traps to elaborate divine contraptions. The spires are usually designed after an aspect the Celestials deemed desirable in any creature attempting to gain access to the Spires and the traps and puzzles are usually designed after such theme.

### Guardians
In case traps and puzzles would not deter possible intruders, divine guardians were placed long ago to protect the spires. However, even divine and immortal beings are not immune to the effects of isolation for millenias. Most of them have turned insane and will attack anyone on sight. 