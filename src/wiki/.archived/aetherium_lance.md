# Aetherium Lance

The Aetherium lance is a weapon designed by [Konrad Von Tinkerstein](../characters/konrad_von_tinkerstein.md). It takes the form of a some sort of cannon and is directly powered by an [Aetherium Core](aetherium_core.md). 

Due to the unstable nature of aetherium energy discharges, the weapon is still in its experimental phase. Konrad managed to achieve some form of control over the discharge by implementing an array of charging coils that control the energy transfer rate from the core to the lance but overheating and misfires are still a common occurence. Due to the strain on its attached core, the weapon can only be fired once or twice every hour. More than that may cause instability in the core itself.

>>>
#### Game mechanics

Range : 300 ft beam (hit)
Modifiers : +1 to Hit (targeting optics can be improved)

On a hit, the Aetherium lance deals 150 point of magical damage. If the target has magic resistance, this resistance is shattered after the first hit until it takes a long rest.

When making an attack with the Lance, roll a d100. The first time it fires, it has a 15% chance of misfire. This misfire chance doubles everytime you fire without letting the weapon cool down for one hour (15% for the first shot, 30% for the next, 60%, 90%). The fifth shot will alaways result in a critical misfire and will cause it to explode as well as the associated aetherium core. 

On a Critical hit, the target takes 150 points of raw magical damage and become vulnerable to any magical attacks regardless of resistances or immunities.
On a Critical failure, roll twice on the misfire table.

If a misfire happen, roll a d20 on the following table

| Roll | Misfire |
| ------ | ------ |
| 1 | The Lance suffers a critical energy feed back. Whoever is manning the weapon takes 50 points of raw magical damage. The associated Aetherium Core takes 3 points of structural damage. The attack automatically fails. The feedback causes imbalance inside of the core and all devices powered by it will suffer malfunctions. |
| 2-5 | The energy is vented around the lance. Every creature in a 15 ft radius around the weapon takes 15 raw magical damage. The attack automatically fails. The associated core takes 1 point of structural damage |
| 6-10 | The energy disperses harmlessly in the air without producing a ray. The attack automatically fails. The associated core takes 1 point of structural damage. | 
| 11-15 | The shot is overcharged and the coils overheat as a result. Add 20 points of damage to the attack. The weapon needs to cool down for 1 rounds before firing again. The associated Aetherium Core takes 1 point of structural damage. |
| 15-19 | The firing safety fails to stop the flow of energy. The associated Aetherium Core takes 1 point of structural damage and you immediatly make another attack. This attack counts toward increasing the misfire risk. If you roll a misfire on this attack, just add 1 point of structural damage to the associated Aetherium Core.
| 20 | All safeties have failed and energy is being drained from the associated Aetherium Core. The weapon damage for this shot is increased to 300 points of raw magical damage. The Core takes 3 points of structural damage. The energies inside the core rise to very unstable levels and all devices powered by it will suffer malfunctions.

>>>