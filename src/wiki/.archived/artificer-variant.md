# Artificer, Variant

## Class Table

### Elemental Bender
| Level | Prof. Bonus | Features | Artificing Points | Spellsink Level | Spellsink Capacity |
|:---:|:---:|:---:|:---:|:---:|:---:|
| 1st | +2 | Artificing, Spellsink | 2 | 1 | - |
| 2nd | +2 | Infuse Item | 3 | 1 | 1 |

## Class Features

### Artificing
At 1st level, you are able to create magical contraptions that cost "Artificing Points" to build, which increase as you level up. 

### Spellsink
At 1st level, you gain the cantrip **Spellsink**, which allows you to "capture" a spell equal to your Spellsink Level from a willing caster, further allowing you to replicate it's effect(s) into an artificed contraption. If the spell costs Chi to cast instead of spell slots, you take your Spellsink Level plus your proficiency bonus. You can choose to either immediately infuse the spell captured into a fitting contraption or "store" it inside your mind up to your Spellsink Capacity.

### Infuse Item
This is the same as the original artificer class, see [here](https://www.dndbeyond.com/classes/artificer#InfuseItem-597298).
