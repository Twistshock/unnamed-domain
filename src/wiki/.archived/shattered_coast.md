# The Shattered Coast

The Shattered Coast is located north of the [Sands of Woe](sands_of_woe.md). It is called as such for the many small islands and reefs that dot the coastal line at this location. The only safe way for any boat to reach the shoreline safely is to follow the Marius Passage that leads to Cliffport. Only the bravest or foolest of captains attempt to navigate the reefs and shallows along the coast.

## Marius Passage

Marius Passage is the name of the relatively safe passage that leads to the shoreline. It was named after Imperial explorer Marius Vicantius who first made landfall on the Shattered Coast.

## Cliffport

Cliffport was established as an Imperial outpost along the coast. It is a safe place for adventurers who wish to explore deeper south and along the coast. The town has also become a resupply point for vessels travelling up and down the coast. The town is protected by an imperial fortress located on one of the island nearby.

## Spider Rock

Spider Rock is located Northwest of Cliffport, along the coast. It is a large rock, rising up from the sea all the way up to the top of the cliffs forming the shoreline. It is connected to the cliffs with an old wooden hanging bridge. The rock is the home a colony of Phase Spiders and generous quantities of silky webs can be seen adorning the rocks and the many caves entrances. The silk can be harvested as a trade good but the presence of the spiders make it very deadly and usually not worthwhile for the locals. 

During great storms, swaths of webs can be torn away by the powerful winds and carried over great distances, even inside of Cliffport. These webs often carry several spiders that were residing in them which explain why the phase spiders are spread so far around Spider Rock. Some say that the Phase Spiders producing the fabled elven silk in the Silver Islands come from this place and were transported across the seas in one of the biggest storm the coast ever saw.

## Storm Islands

The Storm Islands are part of the many islands constituting of the Shattered Coast and are located to the northwest. Many of them are shaped like sharp needles launching toward the skies. Thunder storms are constantly brewing from them and navigation is very hazardous. It is said that the storms are caused by some ancient creatures slumbering in these islands.

## Storm Walls

The Storm Walls is the name given to the range of mountains found south of the Shattered Coast, marking the limit between it and the Sands of Woe. South of these mountains, the sandstorms rage and batter against the mountain. There are a few valleys that daring adventurers can follow to move deeper south but they are also the home of many creatures seeking shelter from the unnatural storms.

### The Howling Valley

The Howling valley is one of the rare passages that leads through the Storm Walls into the Sands of Woe. It is named like this due to the violent winds that rush up the valley from the desert. These winds can sweep adventurers off their feet and carry them far away. Creatures seek shelter in the many caverns that dot the landscape and they will protect their safe spots with ferocity.