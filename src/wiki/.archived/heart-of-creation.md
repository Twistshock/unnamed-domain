# Heart of Creation

An ancient and forgotten artifact from the time the precursors shaped the universe. Locked away in its own pocket dimension. It contains and radiates the very energy of creation itself. It can shape reality as it weaves and unweaves it around itself.

## Precursor Artifact

It is not known if the Heart of Creation was actually created by the precursors of if it was what provided them with their powers. It seems that this information was lost even to them. What is known is that the Heart of Creation was used to shape and forge the planes of existence in the endless void. Its powers holding them aloft in the raging sea where demons teemed, acting as an anchor and focus point.

## Pocket dimension

The heart of Creation requires to be places in a central location where it can access to all the planes at all times. To that end, it was placed in its own pocket dimension or realm. This plane of existence exists solely for the purpose of housing the Heart. The way to access that place was only known to the Precursors.

## Shattering

Some time after the precursor retired from the Universe, the Heart of Creation suffered a cataclysmic event and shattered. This shattering greatly affected its powers and many fragments scattered across the planes. These fragments known as [Shards of Creation](shards_of_creation.md) still hold unimaginable power but as a whole, the Heart is slowly decaying, losing its ability to hold the realms out of the void. 

If the shards were to be reunited with the Heart, it would certainly manage to mend itself and restore its powers.



>>> 
#### GM NOTED - SPOILERS

Maybe that is what the Lonely King did to stop the cataclysm, acquiring a shard and returning it to the heart, strengthening the heart enough to secure the planes for a while. But the power still decays and it will require more soon.
>>>