# Children of the Sands

The children of the sands are a nomadic tribe living on the outskirts of the [Sands of Woe](../locations/sands_of_woe.md). As they travel the desert, they cover their bodies and faces with ample clothing and masks to protect themselves from the sand and the heat. They are usually hostile to anyone cross their path as they have learned not to trust outsiders. 

## Ancestry

The children of the sands were once citizens of the lost empire of Jokasta. After the fall of the empire, a handful of survivors managed to flee north through the sandstorms. They established a new society under the guidance of the moon goddess See'la. 

Centuries passed and the memories of the empire faded with time. The children of the sands only recall this time through myths and legends. They still remember that a great calamity befell their ancestors but refuse to talk about these dark times. Even if they mostly forgot what happened, they still remember the darkness that now lies deep beaneath the Sands of Woe.