 # Blood of the Sun
 
 The Blood of the Sun was a powerful artifact symbolizing the pact made between the ancient Empire of Jokasta and the [Sun God Teck'lict](../characters/tecklict.md). It was a rod made of divine material and radiating with energy from the sun. It was stored in the royal sun temple in the capital city of the old empire, in a room of ivory and gold and placed in a pillar designed specifically to house it. From this pillar, the sun energy was directed toward the sky, projecting its energies over the empire. 
 
 During the fall of the empire, the blood of the sun was consumed by the [Vermillion Serpent](../characters/vermillion_snake.md) and corrupted to become a [Rod of the Void](rod_of_the_void.md).