# The Moon Gate

The Moon Gate is an old structure dating from the early age of the Empire of Jokasta. It is a massive portal-like structure adorned with imperial iconography dedicated to the moon. It lies at the northern limit of the [Sands of Woe](sands_of_woe.md)

## History
When the Empire of Jokasta fell, the sun went dark and sandstorms reduced the once great empire to a lifeless desert. In the midst of destruction and death, a handful of survivors gathered and attempted to escape. However, so fierce where the storms that any attempt to cross the newly created Sands of Woe were only met with certain doom. With the apparent death of the sun god [Teck'lick](../characters/tecklict.md), all hope seemed lost. 

In a deseperate attempt, they turned to the moon goddess [See'la](../characters/seela.md). Her powers although diminished by the influence of the [Vermillion Serpent](../vermillion_snake.md) were sufficient to power an ancient structure dating the early age of the empire. The Moon Gate activated, creating a passage where the sky stood clear long enough for the survivors to escape.