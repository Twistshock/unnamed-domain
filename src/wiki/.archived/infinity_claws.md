# Infinity claws

The Infinity Claws are a powerful artifact forged from a time long forgotten by the Celestials themselves. It is also imbued with powerful demonic energy drawn from the void. The artifact is precariously balancing the opposing energies in itself and a careless wearer would find themselved torn to shreds by the conflicting powers it contains.

It takes the appearance of a pair of gilded golden gauntlets that radiates from golden energy. They are encrusted with diamonds of the highest purity that almost glow a pure white. The fingers of the gauntlets contrast greatly with the rest as they are shaped like the talons of a great bird of prey and made of a material black as night. This dark material also crackles with energy drawn directly from the void and threaten to rip itself apart.


# Purpose

The Infinity Claws were forged to allow the Celestials to manipulate the mystical [Shards of Creation](shards-of-creation.md). The talon like fingers shred the fabric of reality due to the demonic enchantments and can sever the anchoring of the shards into reality, allowing them to be moved freely for a time. This, however, cause a hole in reality itself where the fabric was shredded and can lead to catastrophic consequences.


>>> 
### SPOILERS - GM NOTES - ITEM STATS

END GAME ITEM. IF MISUSED IT WILL BREAK THE WORLD LITERALLY

#### Infinity Claws
*Artifact, melee weapon (unarmed, magical, cursed, blessed)*
**Category:** Items
**Damage:** 3d10
**Modifiers:** +6 Melee attack, +6 Melee Damage
**Item Rarity:** Artifact
**Properties:**
**Weigth:** 2

**Precarious Balance:** 
The Infinity Claws are balancing the energies of the Celestials and of the Demons. If it becomes unbalanced, the wearer of the gauntlets run the risk of being consumed by the swirling energies. 
Every time you make an attack with this artifact, you must make a CON saving throw (DC 18). On a success, you manage to keep the energies balanced at the cost of 1 exhaustion point. On a failure, you are overwhelmed by the energies contained in the claws and the following effects apply :
- If your attack roll was an even number, you take 59 points (7d12 + 10) of Radiant Damage
- If your attack roll was an odd number, you take 59 points (7d12 + 10) of Necrotic Damage
- You gain one point of exhaustion
- You are prone on the ground and stunned for one round as your body is wracked by energy.

**Rend all of reality**
Every time a creature that is not a demon is hit by this weapon, it must make a CON saving throw (DC 18). On a failure, the creature feels reality itself being shredded as it is hit and takes an additionnal 50 (5d10) points of damage as it is removed from reality. On a success, it only takes half damage.
Any creature rendered unconscious by this effect is erased from reality body AND soul. It CANNOT be brought back by any mean, not even a wish spell as the creature do not have any existence in this reality anymore.

>>>
