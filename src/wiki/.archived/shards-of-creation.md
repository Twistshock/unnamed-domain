# Shards of Creation

When the [Heart of Creation](heart-of-creation.md) shattered, some of its shards escaped its pocket dimension and drifted to some of the planes of existence. Quickly, these artifacts of immense power attracted the attention of the Celestials. In their quest to protect the realms from the ever encroaching void, they devised contraptions to harness the very power of Creation that these shards were imbued of.

As they were of Precursor origins, the Celestials were not able to fully understand their purpose nor their function. The only thing they were able to figure out was that these shards, for some reason, were able to reinforce reality itself and to keep the void at bay, strengthening the barrier between the worlds. 

In an effort to house these shards and control their powers, the Celestials built massive structures across the planes to stabilize them and protect them from the demonic incursions.

## Manipulating the Shards

Although the Celestials are some of the powerful creatures to exist, manipulating the shards when they first encountered them proved to be above their abilities. The shards were completely immovable. This is due to the fact that the shards always seek to anchor themselves and become an integral part of the fabric of reality wherever they are. In order to move the shards around, the celestials had to lower themselves to seek the solution into the void itself. By harnessing some of the destructive power they were able to forge artifacts known as [Infinity Claws](infinity_claws.md) which were able to tear reality around the shards, allowing them to be moved freely.


>>>
#### GM NOTES - Shards of Creation in-game effects

The shard looks like a massive crystal, hovering about 5 ft from the ground. It is bathing its surrounding in a bright and soothing light. From a distance, it is possible to discern that this shard is teeming with limitless energy as it holds a fraction of the power that created the planes.

**The light of Creation**
60ft radius Aura, any creature that is not a demon/fiend, is healed to full and completely restored and rejuvenated. All negative effect the character suffered due to curse/afflictions/etc are immediatly removed. They also immediatly benefit from the effects of a long rest.

**Immovable Object**
The shard cannot be moved by any conventionnal means. Attacking the shard with any mean other than void/chaos attacks results in an immediate feedback surge back into the attacker dealing 60 (10d10 + 10) points of pure damage. 

**SPOILERS**
Upon physical contact with a human, said human finds himself in a white space with bright silhouettes looking down to him. They talk about him without really listening to what he says if he tried to communicate. They comment on how one of their child seems to have "made it this far" and that he is "not yet ready". "Should we interfere?". "No they need to learn". "Will they make it". "It is not for us to decide".
This happen in a fraction of second. Onlookers only see him briefly touch the shard.
Other creatures than human cannot come closer than 15 feet from the shard
>>>
