# Valagor the Stormcaller, Herald of the Tempest.

Valagor the Stormcaller is a blue dragon that has made his lair in the highest needle that dot the [Storm Islands](../locations/shattered_coast.md#storm-islands). He has been hiding there since the great slumber was declared by The Patriarch. He has been alive for millenias and was able to attend the very first meeting of what was to become the [Draconic Conclave](../organizations/draconic_conclave.md). He likes to tell that he was amon the first members of the Conclave and that he sat with the Patriarch but other dragons present at that time tends to disagree.

From his lair in the Storm Islands, he calls up the storms and tempests that this place is infamous for. He does this to keep his location concealed and to keep the mortal races away from him. He almost never leave his lair, enjoying the comfort and security it provides far from prying eyes. When he does though, he always conjure the largest storms that makes the whole of the Shattered Coast shake and stir in order to conceal his presence in the clouds.

He is curious with the arrival of the Empire on his shores as to what they seek here but the relatively close proximity of a civilized settlement leaves him concerned and he fears his lair will be discovered at some point. 

## Titles

As a very ancient dragon, Valagor has gathered several titles from mortals and immortals alike :

*  The Stormcaller
*  The Herald of the Tempest
*  Master of the Blue lightning of Kazzaran
*  Wielder of the Heavens
*  The Deadly Hail
*  The Winged Thunder
  

## The Blue Lightning of Kazzaran

Kazzaran (if that is even his real name) is a creature of legends, even for a dragon it can only be referred as myths and folklore. It is said that when the world form, a creature going by the name Kazzaran created lightning. When it was first introduced, Kazzaran wielded it like a lance, throwing it around to show to the others like him. So powerful was it that it sundered part of creation wherever it landed. He was in love with his creation so when he was told to remove his blue lance from creation to prevent its destruction, he instead shattered it into an infinite amount of pieces and scattered them across the newly formed Universe. From this came the lightnings, all containing a minuscule fragment of that very first lightning.

From these myths, it has been assumed that one could be able to capture these infinitesimal fragments and combine them together, to recreate the Blue lightning. For Millenias, Valagor spent time attempting to capture such fragments. Even for a powerful and ancient dragon, such task was nearly impossible and over the thousands of years of its existence, he could only absorb a few. However, the few fragments he got gave him great powers and he now can increase the power of his lightnings. 