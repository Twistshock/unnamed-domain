# Centurion Caius Severus

Centurion Caius Severus has been recently reassigned to [Legate Scipio Tempestus](scipio_tempestus.md)' legion in Cliffport. He was previously assigned to Startertown and managed to escape the destruction caused by the [Infinite Crimson](../organizations/infinite_crimson.md)

Although he does not know much about the cult itself, he is aware of their existence. Due do his dealings with strange and demonic creatures, his experience was seen valuable enough to be part of the expeditionnary force to the Shattered Coast. 