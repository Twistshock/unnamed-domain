# Legate Scipio Tempestus

Legate Scipio Tempestus is the commander of the Legion garrison in [Cliffport](../locations/cliffport.md). His detachment is in charge of securing the Shattered Coast for the Empire and expand its borders. He is dutiful and believes that enforcing the imperial law to the letter will bring civilization to this wretched place.

He currently works with members of the Ordo Witcharium to help clean the monsters of these lands and allow imperial settlers to move out of the safety of Cliffport walls. Although he acknowledge their presence here and is grateful for their assistance, he do have problems with how their methods seems to thread on the grey areas of the law.