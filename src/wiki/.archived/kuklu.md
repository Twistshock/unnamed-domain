# Emperor Kuk'lu the burnt

Emperor Kuk'lu the burnt was the last emperor of the now forgotten empire of Jokasta. He owed his nickname to the fact that in the dying years of the empire, he spent most of his time trying to harness the energies of the artifact known as the [Blood of the Sun](../items/blood_of_the_sun.md) which charred his skin to the point he had to constantly wear healing bandages soothing his burns making him look like a mummy.

He was deposed by [High Priest Tessok Khaliss](tessok_khaliss.md) and the [Ophidian Sect](ophidian_sect.md) and offered as a sacrifice to the [Vermillion Serpent](vermillion_snake.md) when it consumed the Blood of the Sun.