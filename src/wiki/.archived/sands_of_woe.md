# The Sands of Woe

The sands of woe is an ancient desert constantly battered with powerful sandstorms. These storms are so powerful and vicious that they can rip the skin off the bones and send flying the most unprepared of adventurers.

The sands are said to have once been the seat of power of an old empire that is since long gone. The outskirts of the desert is the home of the fierce [Children of the Sand](../organizations/children_of_the_sand.md)

## The Bone Plains 

The Bone plains are located north of the sands of woe and can be accessed from the [Shattered Coast](shattered_coast.md). These sandy plains are riddles with the skeletons of ancient giants and the wind sings it's cold and glacial song through the skulls of these ancient creatures. Many have lost their sanity while venturing through these plains, going as far as to claim that the bones moved and that the giant skulls watched them walk among their remains.

Although still a part of the Sands of Woe, the sandstorms only occasionnally hit the plains but it is still a bad idea to be there while they do.