# Elemental Bender

## Introduction

Bending is the ability to manipulate the form and shape of an element. There are four known bending arts each bending a specific physical element. The four elemental bending arts are based on the four classical elements, water, earth, fire, and air, and each is manipulated through certain martial art styles which are reminiscent of the qualities of the element itself. While bending is traditionally performed through the motions of one's hands and feet, skilled benders are able to effectively manipulate their element with only minimal movement of their bodies, such as by using just their head or torso. In even rarer cases, master benders are able to perform their bending without the aid of any physical movement at all, by instead using sheer focus and force of will, a skill known as psychic bending.

## New Components

| Key | Value | Details                                                     |
| :-: | :---: | :---------------------------------------------------------- |
|  C  |  Chi  | Chi is the special point cost for certain elemental powers. |

#### Calculating Chi

Your Chi is equal to the sum of your Charisma, Dexterity, proficiency level, and Elemental Bender Level, plus how many Chakras you have cleared multiplied by two. In order to use your Chi points, your Chakras must be clear to allow the flow of Chi to run through your body.

## Class Table

### Elemental Bender

| Level | Prof. Bonus | Chi Tapping | Adv. Techniques |                    Features                     |
| :---: | :---------: | :---------: | :-------------: | :---------------------------------------------: |
|  1st  |     +2      |      -      |        -        |       Elemental Bending, Powerful Defense       |
|  2nd  |     +2      |     1d4     |        -        |          Element Redirect, Chi Tapping          |
|  3rd  |     +2      |     1d4     |        -        |                  Traditional Equipment Training, Elemental Block                   |
|  4th  |     +2      |     1d4     |        1        |  Ability Score Improvement, Elemental Mastery   |
|  5th  |     +3      |     1d6     |        1        | Intense Release |
|  6th  |     +3      |     1d6     |        2        | Hybrid Bending |

## Class Features

### Powerful Defense

Beginning at 1st level, while you are wearing no armor and not wielding a shield, your AC equals 10 + your Dexterity modifier + your Charisma modifier.

### Elemental Redirect

At 2nd level, any time you are hit by a ranged, elemental-based attack (this will apply most often to spells, but counters mundane elements as well), you can roll a d20 and add your spell attack bonus. If the result is higher than the attack roll, you are able to redirect the path of the attack within a 160 degree cone behind yourself. If it is equal or lower, you can only redirect it partially within a 45 degree cone behind you and take half damage. This ability works for attacks that require you to make a saving throw as well (such as the Fireball spell). For that, the process is the same, you just follow this process rather than making the saving throw, and the DC of your roll is the save DC, not the attack roll.


### Focus Energy

Whenever you deal elemental damage, you can take a bonus action to deal an extra 1d4 points of whatever elemental damage you deal to your target. This damage will increase as you level up.

### Elemental Block

At 3rd level, any time you are hit by a ranged, elemental-based attack (this will apply most often to spells, but counters mundane elements as well), you can roll a d20 and add your spell attack bonus. If the result is higher than the attack roll, you simply take no damage and any splash damage is halved. If it is equal or lower, you take half damage whilst not affacting any splash damage. This ability works for attacks that require you to make a saving throw as well (such as the Fireball spell). For that, the process is the same, you just follow this process rather than making the saving throw, and the DC of your roll is the save DC, not the attack roll.

### Traditional Equipment Training

Being an elemental bender, you have mastered the art of using your own equipment to augment your bending skills, allowing you to use certain armor and weapons in tandem with your elemental bending. Upon reaching 3rd level, you are able to do the following:

- When you make an attack with any traditional weapons, you can use a bonus action to do a bending technique that normally takes 1 action.
- When you are hit by an attack and have any traditional armor equipped, you can use a reaction to do a bending technique that normally takes 1 bonus action.
- You also gain mastery with any traditional equipment you use.

#### Fire

##### Melee Weapons

- [Dual Scimitars](https://roll20.net/compendium/dnd5e/Items:Scimitar)
- [Glaive](https://roll20.net/compendium/dnd5e/Items:Glaive)
- [Whip](https://roll20.net/compendium/dnd5e/Items:Whip)
- Jii
- Dadao
- Dual Broadswords
- Guan Dao
- Pu Dao
- Chain-Hammer
- Jian
- Kanabo
- Nunchaku

##### Ranged Weapons

- Chain
- Stilletto
- Throwing Knives
- Firearms\*

_\* Fire bending only works with firearms that use gunpowder or another explosive to function_

##### Other

- Cannon
- Bamboo ladders

### Elemental Mastery

This does two things:

1.  **Elemental Redirect+:** Same as Elemental Redirect, except if you roll higher than the attack roll, you can choose to spend 1 Chi point to redirect the attack to any other target within the range of the original attack from yourself. If it is equal or lower, you simply redirect it as normal.
2.  **Elemental Block+:** Same as Elemental Block, except if you roll higher than the attack roll, you can choose to spend 1 Chi point to gamble "absorbing" the elemental attack for bonus Chi equal to it's spell cast level and no higher than your max Chi points, otherwise you will fail in absorbing the elemental attack and will take half damage of the attack. If it is equal or lower, you simply redirect it as normal.

You can only do either of these once per short rest.

### Intense Release

Starting at 5th level, if you are restrained in any way, you can cause your body to reach extremely high temperatures to break the bonds as an action. Metal bonds, like handcuffs, may take several minutes or maybe hours to break, up to the DM. This ability can also be used so that if anyone makes physical contact with you, they will take 1d6 points of Fire damage. This buff lasts for 1 hour, or you can cancel it without taking an action. It can instead be used to create an explosion around you. It has a 15 foot radius centered on you, dealing 6d8 damage to all creatures and objects within the radius besides you. You must complete a long rest before using this feature again.

### Hybrid Bending

Cool new stuff, need to write up! <!-- TODO -->

## Fire Bending (Level 1+)

### Zero-Chi Powers

#### Produce Flame

_Light the way with the light of your fire_

---

- **Casting Time:** 1 action
- **Range:** Self
- **Components:** S
- **Duration:** 10 minutes

A flickering flame appears in your hand. The flame remains there for the duration and harms neither you nor your equipment. The flame sheds bright light in a 10-foot radius and dim light for an additional 10 feet. The spell ends if you dismiss it as an action or if you cast it again. You can also attack with the flame, although doing so ends the spell. When you cast this spell, or as an action on a later turn, you can hurl the flame at a creature within 30 feet of you. Make a ranged spell attack. On a hit, the target takes 1d8 fire damage. This spell’s damage increases by 1d8 when you reach 5th level (2d8), 11th level (3d8), and 17th level (4d8). This technique can be amplified with chi!

##### Chi Amplification

| Extra Chi |  Damage  |
| :-------: | :------: |
|     2     | 1d8 + 4  |
|     5     | 2d8 + 4  |
|     8     | 2d12 + 6 |

_Derived from [Dancing Lights](https://roll20.net/compendium/dnd5e/Dancing%20Lights)_

#### Dancing Embers

_Create floating embers to light the way_

---

- **Casting Time:** 1 action
- **Range:** 60 feet
- **Components:** S
- **Duration:** Instantaneous or 1 hour

You create up to four torch-sized lights within range, making them appear as torches, lanterns, or glowing orbs that hover in the air for the duration. You can also combine the four lights into one glowing vaguely humanoid form of Medium size. Whichever form you choose, each light sheds dim light in a 10-foot radius.

As a bonus action on your turn, you can move the lights up to 60 feet to a new spot within range. A light must be within 20 feet of another light created by this spell, and a light winks out if it exceeds the spell’s range.

_Derived from [Dancing Lights](https://roll20.net/compendium/dnd5e/Dancing%20Lights)_

#### Control Flames

_Command fire to do your bidding_

---

- **Casting Time:** 1 action
- **Range:** 60 feet
- **Components:** S
- **Duration:** Instantaneous or 1 hour

You choose a nonmagical flame that you can see within range and that fits within a 10-foot cube. You affect it in one of the following ways: You instantaneously expand the flame 5 feet in one direction, provided that wood or other fuel is present in the new location. You instantaneously extinguish the flames within the cube. You double or halve the area of bright light and dim light cast by the flame, change its color, or both. The change lasts for 1 hour. You cause simple shapes—such as the vague form of a creature, an inanimate object, or a location—to appear within the flames and animate as you like. The shapes last for 1 hour. If you cast this spell multiple times, you can have up to three non-instantaneous effects created by it active at a time, and you can dismiss such an effect as an action.

_Derived from [Control Flames](https://roll20.net/compendium/dnd5e/Control%20Flames)_

#### Fire Bolt

_Basic firebender attack_

---

- **Casting Time:** 1 action
- **Range:** 140 feet
- **Components:** S
- **Duration:** Instantaneous

You hurl a mote of fire at a creature or object within range. Make a ranged spell attack against the target. On a hit, the target takes 1d10 fire damage. A flammable object hit by this spell ignites if it isn’t being worn or carried.

_Derived from [Fire Bolt](https://roll20.net/compendium/dnd5e/Fire%20Bolt)_

### One-Chi Powers

#### Pillar of fire

_Manifest a self-sustaining pillar of flame_

---

- **Casting Time:** 1 action
- **Range:** 60 feet
- **Components:** V, S, C (1)
- **Duration:** 1 minute

You summon a pillar of fire that has a diameter of 5 feet, causing 1d6 fire damage to anything caught within it.

_Non-derived_

#### Fire Aspect

_Because a flaming sword is fucking cool_

---

- **Casting Time:** 1 bonus action
- **Range:** Touch
- **Components:** V, S. C (1)
- **Duration:** 10 minutes

You augment an item or weapon with the element of flame, the heat and fire itself doesn't damage the thing getting augmented. Any attacks made with said item or weapon has an extra 1d6 fire damage.

_Non-derived_

### Two-Chi Powers

#### Fireskin

_Protection using the power of flame_

---

- **Casting Time:** 1 action
- **Range:** Touch
- **Components:** V, C (2)
- **Duration:** Concentration, Up to 1 hour

You touch a willing creature. Until the spell ends, the target’s skin/armour is enveloped in magical flame, and the target’s AC can’t be less than 16, regardless of what kind of armor it is wearing. Any contact with the fireskin, such as melee attacks, causes 1d6 fire damage. And the target also takes half damage from all fire-based attacks whilst wearing the fireskin.

_Derived from [Barkskin](https://roll20.net/compendium/dnd5e/Barkskin)_

#### Fire Daggers

_A fire warrior's backup-backup weapons_

---

- **Casting Time:** 1 action
- **Range:** Touch
- **Components:** C (2)
- **Duration:** 10 minutes

You gain two weightless fire-based weapons that you are a master of. Each fire dagger acts exactly the same as a regular dagger, except it does fire damage instead. Both daggers can be turned into a firebolt with a bonus action, or done seperately. This technique can be amplified with chi!

##### Chi Amplification

| Extra Chi | Weapon Modifier  |
| :-------: | :-----: |
|     2     | +1 |
|     4     | +2 |
|     6     | +3 |

_Non-derived_

#### Scorching File

_Ever heard of walking on coals? Yeah, this is worse_

---

- **Casting Time:** 1 action
- **Range:** 30 feet
- **Components:** V, S, C (2)
- **Duration:** Instantaneous

A line of roaring flame 30 feet long and 5 feet wide emanates from you in a direction you choose. Each creature in the line must make a Dexterity saving throw. A creature takes 3d8 fire damage on a failed save, or half as much damage on a successful one.

_Derived from [Aganazzar's Scorcher](https://roll20.net/compendium/dnd5e/Aganazzar's%20Scorcher)_

#### Flaming Sphere

_Don't tell Zyrt', but it's like a mini-fireball owo_

---

- **Casting Time:** 1 action
- **Range:** 60 feet
- **Components:** V, S, C (2)
- **Duration:** Concentration, Up to 1 minute

A 5-foot-diameter sphere of fire appears in an unoccupied space of your choice within range and lasts for the duration. Any creature that ends its turn within 5 feet of the sphere must make a Dexterity saving throw. The creature takes 2d6 fire damage on a failed save, or half as much damage on a successful one. As a bonus action, you can move the sphere up to 30 feet. If you ram the sphere into a creature, that creature must make the saving throw against the sphere’s damage, and the sphere stops moving this turn. When you move the sphere, you can direct it over barriers up to 5 feet tall and jump it across pits up to 10 feet wide. The sphere ignites flammable objects not being worn or carried, and it sheds bright light in a 20-foot radius and dim light for an additional 20 feet.

_Derived from [Flaming Sphere](https://roll20.net/compendium/dnd5e/Flaming%20Sphere)_

#### Heat Metal

_For when you're facing a tank, essentially_

---

- **Casting Time:** 1 action
- **Range:** 60 feet
- **Components:** S, C (2)
- **Duration:** Concentration, Up to 1 minute

Choose a manufactured metal object, such as a metal weapon or a suit of heavy or medium metal armor, that you can see within range. You cause the object to glow red-hot. Any creature in physical contact with the object takes 2d8 fire damage when you cast the spell. Until the spell ends, you can use a bonus action on each of your subsequent turns to cause this damage again. If a creature is holding or wearing the object and takes the damage from it, the creature must succeed on a Constitution saving throw or drop the object if it can. If it doesn’t drop the object, it has disadvantage on attack rolls and ability checks until the start of your next turn.

_Derived from [Heat Metal](https://roll20.net/compendium/dnd5e/Heat%20Metal)_

### Three-Chi Powers

#### Dragon's Breath

_Your voice becomes a torrent of flame_

---

- **Casting Time:** 1 bonus action
- **Range:** Touch
- **Components:** V, C (3)
- **Duration:** Up to 1 minute

You touch one willing creature and imbue it with the power to spew magical energy from its mouth, provided it has one. Until the spell ends, the creature can use an action to exhale energy of the chosen type in a 15-foot cone. Each creature in that area must make a Dexterity saving throw, taking 3d6 fire damage on a failed save, or half as much damage on a successful one.

_Derived from [Dragon's Breath](https://roll20.net/compendium/dnd5e/Dragon's%20Breath)_

#### Scorching Ray

_For when you're facing a tank, essentially_

---

- **Casting Time:** 1 action
- **Range:** 120 feet
- **Components:** V, S, C (3)
- **Duration:** Instantaneous

You create three rays of fire and hurl them at targets within range. You can hurl them at one target or several. Make a ranged spell attack for each ray. On a hit, the target takes 2d8 fire damage. This technique can be amplified with chi!

##### Chi Amplification

| Extra Chi | Damage  | # of Rays |
| :-------: | :-----: | :-------: |
|     1     | 2d8 + 2 |     3     |
|     3     | 2d8 + 4 |     4     |
|     7     |   2d8   |     6     |

_Derived from [Scorching Ray](https://roll20.net/compendium/dnd5e/Scorching%20Ray)_

#### Flame Whip

_Like Indiana Jones, but better, because of fire_

---

- **Casting Time:** 1 action
- **Range:** 120 feet
- **Components:** V, S, C (3)
- **Duration:** Concentration, Up to 10 minutes

You evoke a fiery blade in your free hand. The blade is similar in size and shape to a scimitar, and it lasts for the duration. If you let go of the blade, it disappears, but you can evoke the blade again as a bonus action. You can use your action to make a melee spell attack with the fiery blade. On a hit, the target takes 3d6 fire damage. The flaming blade sheds bright light in a 10-foot radius and dim light for an additional 10 feet.

##### Chi Amplification

| Extra Chi | Damage  |
| :-------: | :-----: |
|     2     | 3d6 + 3 |
|     6     | 4d6 + 4 |
|     10     |   5d6 + 2   |

_Derived from [Flame Blade](https://roll20.net/compendium/dnd5e/Flame%20Blade)_

### Four-Chi Powers

#### Wall of Fire

_Summon a tremendous wall of fire_

---

- **Casting Time:** 1 action
- **Range:** 120 feet
- **Components:** V, S, C (4)
- **Duration:** Concentration, Up to 1 minute

You create a wall of fire on a solid surface within range. You can make the wall up to 60 feet long, 20 feet high, and 1 foot thick, or a ringed wall up to 20 feet in diameter, 20 feet high, and 1 foot thick. The wall is opaque and lasts for the duration. When the wall appears, each creature within its area must make a Dexterity saving throw. On a failed save, a creature takes 5d8 fire damage, or half as much damage on a successful save. One side of the wall, selected by you when you cast this spell, deals 5d8 fire damage to each creature that ends its turn within 10 feet of that side or inside the wall. A creature takes the same damage when it enters the wall for the first time on a turn or ends its turn there. The other side of the wall deals no damage.

_Derived from [Wall Of Fire](https://roll20.net/compendium/dnd5e/Wall%20Of%20Fire)_

### Six-Chi Powers

#### Fireball

_Protection using the power of flame_

---

- **Casting Time:** 1 action
- **Range:** 150 feet
- **Components:** V, S, C (6)
- **Duration:** Instantaneous

A bright streak flashes from your pointing finger to a point you choose within range and then blossoms with a low roar into an explosion of flame. Each creature in a 20-foot-radius sphere centered on that point must make a Dexterity saving throw. A target takes 8d6 fire damage on a failed save, or half as much damage on a successful one. The fire spreads around corners. It ignites flammable objects in the area that aren’t being worn or carried. This technique can be amplified with chi!

**Warning:** The caster of this technique must succeed a Dexterity saving throw equal to the difference between the caster's spell save DC minus their elemental bender level, failing this save will cause the caster to take half of the damage dealt to the target.

##### Chi Amplification

| Extra Chi | Damage  |      AoE       |
| :-------: | :-----: | :------------: |
|     6     | 8d6 + 6 | 25-foot sphere |
|    10     |  10d8   | 30-foot sphere |
|    12     |  10d12  | 40-foot sphere |

_Derived from [Fireball](https://roll20.net/compendium/dnd5e/Fireball)_

#### Immolation

_Envelope a target in fire_

---

- **Casting Time:** 1 action
- **Range:** 90 feet
- **Components:** V, S, C (6)
- **Duration:** Concentration, Up to 1 minute

Flames whip out from your hands and wreathe one creature you can see within range. The target must make a Dexterity saving throw. It takes 8d6 fire damage on a failed save, or half as much damage on a successful one. On a failed save, the target also burns for the spell’s duration. The burning target sheds bright light in a 30-foot radius and dim light for an additional 30 feet. At the end of each of its turns, the target repeats the saving throw. It takes 4d6 fire damage on a failed save, and the spell ends on a successful one. These magical flames can’t be extinguished by nonmagical means. If damage from this spell kills a target, the target is turned to ash.

_Derived from [Immolation](https://roll20.net/compendium/dnd5e/Immolation)_

## Lightning Bending (Level 4+)

### Zero-Chi Powers

#### Shocking Grasp

_My dad always said I needed a firmer handshake_

---

- **Casting Time:** 1 action
- **Range:** Touch
- **Components:** None
- **Duration:** Instantaneous

Lightning springs from your hand to deliver a shock to a creature you try to touch. Make a melee spell attack against the target. You have advantage on the attack roll if the target is wearing armor made of metal. On a hit, the target takes 1d8 lightning damage, and it can’t take reactions until the start of its next turn. This technique can be amplified with chi!

##### Chi Amplification

| Extra Chi | Damage  |
| :-------: | :-----: |
|     1     | 1d8 + 4 |
|     2     |   2d8   |
|     4     |   3d8   |
|    10     |   4d8   |

_Derived from [Shocking Grasp](https://roll20.net/compendium/dnd5e/Shocking%20Grasp)_

#### Lightning Bolt

_Absolutely Shocking!!_

---

- **Casting Time:** 1 action
- **Range:** 30 Feet
- **Components:** S
- **Duration:** Instantaneous

A beam of crackling, blue energy lances out toward a creature within range, forming an unstable arc of lightning between you and the target. Make a ranged spell attack against that creature. On a hit, the target takes 1d12 lightning damage. This technique can be amplified with chi!

##### Chi Amplification

| Extra Chi |  Damage  |
| :-------: | :------: |
|     1     | 1d12 + 4 |
|     3     | 2d12 + 4 |
|     7     |   4d12   |

_Derived from [Witch Bolt](https://roll20.net/compendium/dnd5e/Witch%20Bolt)_

### One-Chi Powers

#### Lightning Aspect

_You can make stun batons with this!_

---

- **Casting Time:** 1 bonus action
- **Range:** Touch
- **Components:** V, S. C (1)
- **Duration:** 10 minutes

You augment an item or weapon with the element of lightning, the element itself doesn't damage the thing getting augmented. Any attacks made with said item or weapon has an extra 1d8 lightning damage and cause the target to break concentration on a failed constitution save.

_Non-derived_

### Two-Chi Powers

#### Lightning Orb

_It's like a taser, but lethal!_

---

- **Casting Time:** 1 action
- **Range:** 90 Feet
- **Components:** V, S, C (2)
- **Duration:** Instantaneous

You hurl a 4-inch-diameter sphere of energy at a creature that you can see within range by making a ranged spell attack against the target. If the attack hits, the creature takes 3d8 lightning damage. This technique can be amplified with chi!

##### Chi Amplification

| Extra Chi | Damage  |
| :-------: | :-----: |
|     1     | 3d8 + 6 |
|     3     | 4d8 + 2 |
|     6     |   5d8   |
|    10     |   6d8   |

_Derived from [Chromatic Orb](https://roll20.net/compendium/dnd5e/Chromatic%20Orb)_

### Six-Chi Powers

#### Chain Lightning

_Mass-electricution never looked so good_

---

- **Casting Time:** 1 action
- **Range:** 150 Feet
- **Components:** V, S, C (6)
- **Duration:** Instantaneous

You create a bolt of lightning that arcs toward a target of your choice that you can see within range. Three bolts then leap from that target to as many as three other targets, each of which must be within 30 feet of the first target. A target can be a creature or an object and can be targeted by only one of the bolts. A target must make a Dexterity saving throw. The target takes 12d4 lightning damage on a failed save, or half as much damage on a successful one.

**Warning:** The caster of this technique must succeed a Dexterity saving throw equal to the difference between the caster's spell save DC minus their elemental bender level, failing this save will cause the caster to take half of the damage dealt to the target.

_Derived from [Chain Lightning](https://roll20.net/compendium/dnd5e/Chain%20Lightning)_

## Earth Bending (Level 1+)

### Zero-Chi Powers

#### Mold Earth

_The Earth bends to your will!_

---

- **Casting Time:** 1 action
- **Range:** 30 feet
- **Components:** S
- **Duration:** Instantaneous or 1 hour

You choose a portion of dirt or stone that you can see within range and that fits within a 5-foot cube. You manipulate it in one of the following ways:

If you target an area of loose earth, you can instantaneously excavate it, move it along the ground, and deposit it up to 5 feet away. This Movement doesn’t involve enough force to cause damage.

You cause shapes, colors, or both to appear on the dirt or stone, spelling out words, creating images, or shaping patterns. The changes last for 1 hour.

If the dirt or stone you target is on the ground, you cause it to become difficult terrain. Alternatively, you can cause the ground to become normal terrain if it is already difficult terrain. This change lasts for 1 hour.

If you cast this spell multiple times, you can have no more than two of its non-instantaneous Effects active at a time, and you can dismiss such an effect as an action.

_Derived from [Mold Earth](https://roll20.net/compendium/dnd5e/Mold%20Earth)_

### One-Chi Powers

#### Earth Tremor

_Because mini-earthquakes are pretty cool_

---

- **Casting Time:** 1 action
- **Range:** 10 feet
- **Components:** V, S, C (1)
- **Duration:** Instantaneous

You cause a tremor in the ground within range. Each creature other than you in that area must make a Dexterity saving throw. On a failed save, a creature takes 1d6 bludgeoning damage and is knocked prone. If the ground in that area is loose earth or stone, it becomes difficult terrain until cleared, with each 5-foot-diameter portion requiring at least 1 minute to clear by hand. This technique can be amplified with chi!

##### Chi Amplification

| Extra Chi | Damage  |
| :-------: | :-----: |
|     1     | 2d6 |
|     3     | 2d8 + 4 |
|     6     |   4d8   |
|    10     |   8d8   |

_Derived from [Earth Tremor](https://roll20.net/compendium/dnd5e/Earth%20Tremor)_

### Two-Chi Powers

#### Earthbinds

_Bring the earth to the enemy!_

---

- **Casting Time:** 1 action
- **Range:** 300 feet
- **Components:** V, S, C (2)
- **Duration:** Concentration, Up to 1 minute

Choose one creature you can see within range. Thick ropes of earth loop around the creature from the ground. The target must succeed on a Strength saving throw, or its speed (if any) is reduced to 0 feet for the spell’s duration. An airborne creature affected by this spell safely descends at 60 feet per round until it reaches the ground or the spell ends. Whilst on the ground, the creature is considered grappled.

_Derived from [Earthbind](https://roll20.net/compendium/dnd5e/Earthbind)_

#### Earthen Grasp

_Did somebody call for sandman?_

---

- **Casting Time:** 1 action
- **Range:** 30 feet
- **Components:** V, S, C (2)
- **Duration:** Concentration, Up to 1 minute

You choose a 5-foot-square unoccupied space on the ground that you can see within range. A Medium hand made from compacted soil rises there and reaches for one creature you can see within 5 feet of it. The target must make a Strength saving throw. On a failed save, the target takes 2d6 bludgeoning damage and is restrained for the spell’s duration. As an action, you can cause the hand to crush the restrained target, which must make a Strength saving throw. The target takes 2d6 bludgeoning damage on a failed save, or half as much damage on a successful one. To break out, the restrained target can use its action to make a Strength check against your spell save DC. On a success, the target escapes and is no longer restrained by the hand. As an action, you can cause the hand to reach for a different creature or to move to a different unoccupied space within range. The hand releases a restrained target if you do either.

_Derived from [Maximillian's Earthen Grasp](https://roll20.net/compendium/dnd5e/Maximillian's%20Earthen%20Grasp)_

#### Stoneskin

_Big rock-boy hours_

---

- **Casting Time:** 1 action
- **Range:** Touch
- **Components:** V, S, C (2)
- **Duration:** Concentration, Up to 1 hour

This spell turns the flesh of a willing creature you touch as hard as stone. Until the spell ends, the target has resistance to nonmagical bludgeoning, piercing, and slashing damage, as well as any elemental-based damage. Whilst wearing stoneskin, the user is considered encumbered, unless they are already encumbered, then they are heavily encumbered.

_Derived from [Stoneskin](https://roll20.net/compendium/dnd5e/Stoneskin)_

### Four-Chi Powers

#### Erupting Earth

_What happens when you replace lava in an erupting volcano with big rocks?_

---

- **Casting Time:** 1 action
- **Range:** 120 feet
- **Components:** V, S, C (4)
- **Duration:** Instantaneous

Choose a point you can see on the ground within range. A fountain of churned earth and stone erupts in a 20-foot cube centered on that point. Each creature in that area must make a Dexterity saving throw. A creature takes 3d12 bludgeoning damage on a failed save, or half as much damage on a successful one. Additionally, the ground in that area becomes difficult terrain until cleared. Each 5-foot-square portion of the area requires at least 1 minute to clear by hand. This technique can be amplified with chi!

##### Chi Amplification

| Extra Chi | Damage  | Cube Size |
| :-------: | :-----: | :-: |
|     1     | 3d12 + 4 | 20-foot |
|     3     | 4d12 | 20-foot |
|     5     | 5d12 | 30-foot |
|     9     | 5d12 | 45-foot |

_Derived from [Erupting Earth](https://roll20.net/compendium/dnd5e/Erupting%20Earth)_

### Six-Chi Powers

#### Move Earth

_Pure earthbending at it's best_

---

- **Casting Time:** 1 action
- **Range:** 120 feet
- **Components:** V, S, C (6)
- **Duration:** Concentration, Up to 2 hours

Choose an area of terrain no larger than 40 feet on a side within range. You can reshape dirt, sand, or clay in the area in any manner you choose for the duration. You can raise or lower the area’s elevation, create or fill in a trench, erect or flatten a wall, or form a pillar. The extent of any such changes can’t exceed half the area’s largest dimension. So, if you affect a 40-foot square, you can create a pillar up to 20 feet high, raise or lower the square’s elevation by up to 20 feet, dig a trench up to 20 feet deep, and so on. It takes 10 minutes for these changes to complete.
At the end of every 10 minutes you spend concentrating on the spell, you can choose a new area of terrain to affect.

Because the terrain’s transformation occurs slowly, creatures in the area can’t usually be trapped or injured by the ground’s movement.

This spell can’t manipulate natural stone or stone construction. Rocks and structures shift to accommodate the new terrain. If the way you shape the terrain would make a structure unstable, it might collapse.

Similarly, this spell doesn’t directly affect plant growth. The moved earth carries any plants along with it.

_Derived from [Move Earth](https://roll20.net/compendium/dnd5e/Move%20Earth)_

### Ten-Chi Powers

#### Earthquake

_Bring the rage of the earth to bare with this one_

---

- **Casting Time:** 1 action
- **Range:** 500 feet
- **Components:** V, S, C (10)
- **Duration:** Concentration, Up to 1 minute

You create a seismic disturbance at a point on the ground that you can see within range. For the duration, an intense tremor rips through the ground in a 100-foot-radius circle centered on that point and shakes creatures and structures in contact with the ground in that area.

The ground in the area becomes difficult terrain. Each creature on the ground that is concentrating must make a Constitution saving throw. On a failed save, the creature’s concentration is broken.

When you cast this spell and at the end of each turn you spend concentrating on it, each creature on the ground in the area must make a Dexterity saving throw. On a failed save, the creature is knocked prone.

This spell can have additional effects depending on the terrain in the area, as determined by the DM.

Fissures. Fissures open throughout the spell’s area at the start of your next turn after you cast the spell. A total of 1d6 such fissures open in locations chosen by the DM. Each is 1d10 × 10 feet deep, 10 feet wide, and extends from one edge of the spell’s area to the opposite side. A creature standing on a spot where a fissure opens must succeed on a Dexterity saving throw or fall in. A creature that successfully saves moves with the fissure’s edge as it opens.

A fissure that opens beneath a structure causes it to automatically collapse (see below).

Structures. The tremor deals 50 bludgeoning damage to any structure in contact with the ground in the area when you cast the spell and at the start of each of your turns until the spell ends. If a structure drops to 0 hit points, it collapses and potentially damages nearby creatures. A creature within half the distance of a structure’s height must make a Dexterity saving throw. On a failed save, the creature takes 5d6 bludgeoning damage, is knocked prone, and is buried in the rubble, requiring a DC 20 Strength (Athletics) check as an action to escape. The DM can adjust the DC higher or lower, depending on the nature of the rubble. On a successful save, the creature takes half as much damage and doesn’t fall prone or become buried.

_Derived from [Earthquake](https://roll20.net/compendium/dnd5e/Earthquake)_

## Metal Bending (Level 4+)

### Two-Chi Powers

#### Metalskin

_I. AM. IRON MAN!_

---

- **Casting Time:** 1 action
- **Range:** 30 feet
- **Components:** V, S, C (2), M (Enough metal to cover the creature, see description)
- **Duration:** Concentration, Up to 10 minutes

You target one willing creature, including yourself, and call metal to them equal to their size, their Armor Class is 17 plus your Dexterity modifier and they do not suffer a penalty to their movement speed.

##### Component Table

| Tartget Size | Amount of Metal Required (cubic feet)  | Extra Chi Required (if any) |
| :-------: | :-----: | :-----: |
|     Tiny     | 0.25 | 2 |
|     Small     | 0.75 | 1 |
|     Medium     |   1   | 0 |
|    Large     |   4   | 2 |
|    Huge     |   9   | 6 |
|    Gargantuan     |   16   | 10 |

###### Average "Medium" Human Calc:
![](https://cdn.discordapp.com/attachments/385124268431638539/620612577613053969/unknown.png)

_Non-derived_

### Ten-Chi Powers

#### Blade Barrier

_A fucking wall of swords, man!_

---

- **Casting Time:** 1 action
- **Range:** 90 feet
- **Components:** V, S, C (10)
- **Duration:** Concentration, Up to 10 minutes

You create a vertical wall of whirling, razor-sharp blades made of magical energy. The wall appears within range and lasts for the duration. You can make a straight wall up to 100 feet long, 20 feet high, and 5 feet thick, or a ringed wall up to 60 feet in diameter, 20 feet high, and 5 feet thick. The wall provides three-quarters cover to creatures behind it, and its space is difficult terrain.

When a creature enters the wall’s area for the first time on a turn or starts its turn there, the creature must make a Dexterity saving throw. On a failed save, the creature takes 6d10 slashing damage. On a successful save, the creature takes half as much damage.

_Derived from [Blade Barrier](https://roll20.net/compendium/dnd5e/Blade%20Barrier)_
