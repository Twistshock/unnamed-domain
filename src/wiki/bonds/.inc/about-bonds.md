## About Bonds

### Summary

A bond is the result of a powerful arcanic process intended to "borrow" power from a celestial, demonic, or other magically-potent entity for a price of some sort. The process is usually incredibly expensive and requires a great deal of effort to pull off, however, some find themselves bonded without even trying. In these cases, instead of the mortal trying to bond to the entity, the bond is typically forced onto the mortal from a higher being.

### Levelling

Once a bond has been made with an entity, the connection level of the bond can be increased to gain more power in return, but at a higher cost. The bond levels are as follows, from weakest to strongest connection:

1. **Bonded:** This is the first and weakest level of bonding.
2. **Secured:** Through some sort of ritual or offering, the connection to the entity is strengthened to allow the host to start pulling power through the bond.
3. **Allowed:** The entity has noticed the host's bond and permits it to exist, this in itself allows the host to draw more power through the bond without annoying the entity.
5. **Pledged:** The host continues to sacrifice more their soul to the entity, the bond strengthens again, increasing it's reliability to pull greater power.
4. **Warranted:** The entity recnognises the host as a worthy bond-partner, unlocking powerful secrets for the host to exploit.
6. **Committed:** The host makes an ultimate offering to the entity, this is typically the last possible level of bonding to most mortals and opens one's soul to the entity.
7. **Taken:** This is the final and strongest bond, as the entity is now permanently a part of the host's soul, allowing the host to reach near to the same power of the entity itself.
