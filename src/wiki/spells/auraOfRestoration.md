# Aura of Restoration

_Create a bubble of healing around yourself_

---

- **Casting Time:** 2 actions
- **Range:** Self
- **Components:** V, S, M (A red or pink coloured gem worth at least 1 gold)
- **Duration:** Concentration, Up to 10 minutes

Life-preserving energy radiates from you in an aura with a 30-foot radius.  Until the spell ends, the aura moves with you, centered on you. Each nonhostile creature in the aura (including you) regains 4 hit points when it starts its turn in the aura, and its hit point maximum can’t be reduced.

_Derived from [Aura of Life](https://www.dndbeyond.com/spells/aura-of-life) (dndbeyond.com)_
