# J'elrun's Gaze

_Use the power of the trickster to deter attackers_

---

- **Casting Time:** 1 action
- **Range:** 30 feet
- **Components:** V, S, M ()
- **Duration:** Concentration, 10 Minutes

You attempt to charm a humanoid you can see within range. It must make a Wisdom saving throw, and does so with advantage if you or your companions are fighting it. If it fails the saving throw, it is charmed by you until the spell ends or until you or your companions do anything harmful to it. The charmed creature regards you as a friendly acquaintance. When the spell ends, the creature knows it was charmed by you.

_Derived from [Charm Person](https://www.dndbeyond.com/spells/charm-person) (dndbeyond.com)_
