# Chains of Vena

_Summon holy chains to bind evil before you strike_

---

- **Casting Time:** 1 action
- **Range:** 90 feet
- **Components:** V, S, M (A metal lock of any variety)
- **Duration:** Concentration, up to 1 minute

Animated, golden chains fill a 20-foot square on ground that you can see within range. For the duration, these chains turn the ground in the area into difficult terrain.

When a creature enters the affected area for the first time on a turn or starts its turn there, the creature must succeed on a Dexterity saving throw or take 2d6 radiant damage plus 1d8 fire damage and be restrained by the chains until the spell ends. A creature that starts its turn in the area and is already restrained by the chains takes 1d8 fire damage.

A creature restrained by the chains can use its action to make a Strength or Dexterity check (its choice) against your spell save DC. On a success, it frees itself.

_Derived from [Black Tentacles](https://www.dndbeyond.com/spells/black-tentacles) (dndbeyond.com)_
