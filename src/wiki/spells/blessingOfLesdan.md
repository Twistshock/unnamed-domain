# Blessing of Lesdan

_Protect the innocent from the forces of evil_

---

- **Casting Time:** 1 reaction
- **Range:** 60 feet
- **Components:** V, S, M (A single unblossomed rose)
- **Duration:** 10 minutes

A barrier of divine energy appears and protects the target of this spell. Until the start of your next turn, you have a +8 bonus to AC, including against the triggering attack.

_Derived from [Shield](https://www.dndbeyond.com/spells/shield) (dndbeyond.com)_
