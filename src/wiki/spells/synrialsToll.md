# Synrial's Toll

_Call upon the powers of death to reap the undead_

---

- **Casting Time:** 1 action
- **Range:** 30 feet
- **Components:** V, S, M (A handful of sand)
- **Duration:** Instantaneous

Each undead that can see or hear you must make a Wisdom saving throw. If the creature fails its saving throw, it is destroyed.

_Derived from [Destroy Undead](https://www.dndbeyond.com/classes/cleric#ClassFeatures) (dndbeyond.com)_
