# Hera, Goddess of Life

## Draft Notes

- Herala appears to all as an ethereal humanoid figure representing a woman with no facial features
- Herala's Archangels are committed to the preservation of life in all it's forms
- Herala's Archangels are:
  - [Bellari](bellari.md), Archangel of Restoration

## Reference Image

![](/.assets/img/herala.png)

## Music Choices

<iframe src="https://www.youtube.com/embed/ubZhS-0l4wk" frameborder="0"></iframe>
