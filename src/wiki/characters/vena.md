# Vena, Archangel of Inquisition

## Draft Notes

- Vena is an [Archangel](/concepts/archangel.md) of both [Tormial](tormial.md) (God of Peace) and [Atros](atros.md) (God of War)
- Vena's symbol is that of a skull adorned with black roses
- Vena uses a spear with a very large blade, akin to that of a glaive
- Angels of Vena are great hunters of evil that scour the lands, searching for evil so that they may strike it down and destroy it
- Followers of Vena can use one of two spells depending on their alignment:
  - **Lawful:** [Chains of Vena](/spells/chainsOfVena.md) to bind evil in place before they strike them down
  - **Chaotic:** [Vena's Anger](/spells/venasAnger.md) to embue their weapon with the terrentrous fire of Vena's wrath

## Reference Image

![](/.assets/img/vena.png)
