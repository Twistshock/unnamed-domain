# Jallak Maa'al, Goddess of Chaos and Change

## Draft Notes

- Jallak Maa'al takes many forms, and has only ever been recorded once appearing as a black and white haired winged woman endorned with arcanic symbols
- Jallak Maa'al's Archangels ensure that the realms continue to change and typically encite those changes directly through deceit and trickery
- Jallak Maa'al's Archangels are:
  - [J'elrun](jelrun.md), Archangel of Trickery

## Reference Image

![](/.assets/img/jalaakmaaal.png)

## Music Choices

<iframe src="https://www.youtube.com/embed/JYFU_RiefKk" frameborder="0"></iframe>
<iframe src="https://www.youtube.com/embed/WEEltzTwbE4" frameborder="0"></iframe>
