# Lesdan, Archangel of Peace and Prosperity

## Draft Notes

- Lesdan is an [Archangel](/concepts/archangel.md) of [Tormial](tormial.md), God of Peace
- Lesdan's symbol is that of a shield adorned with blossoming roses
- Lesdan uses a shield to protect the innocent from the wicked
- Lesdan wields no weapon, they instead use pacification and banishment spells to strike down the wicked
- Angels of Lesdan are famed appear in a person's ultimate time of need where a good and innocent person is about to be struck down by a great evil
- Priests of Lesdan can use [Blessing of Lesdan](/spells/blessingOfLesdan.md) to protect individuals from harm

## Reference Image

![](/.assets/img/lesdan.png)
