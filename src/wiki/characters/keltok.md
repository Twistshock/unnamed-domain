# Keltok, God of Death

## Draft Notes

- Keltok usually takes the form of a tall and brooding cloaked figure wielding a sythe blade attached to a chain.
- Keltok's Archangels are devoted to the collection of souls that are meant to pass onto the afterlife.
- Keltok's Archangels are:
  - [Synrial](synrial.md), Archangel of Reaping

## Reference Image

![](/.assets/img/keltok.png)
