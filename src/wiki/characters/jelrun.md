# J'elrun, Archangel of Trickery

[[TOC]]

## Draft Notes

- J'elrun is an [Archangel](/concepts/archangel.md) of [Keltok](keltok.md), Goddess of Chaos and Change
- J'elrun's symbol resembles a theatre mask split into two sides, the left side is clear of imperfections whereas the right is cracked and engraved with arcanic runes
- J'elrun wields many extravagant weapons, but has never been seen to use them
- Angels of J'elrun nonesensically sew the seeds of change and discord in societies arouns the world
- Priests of J'elrun can use [J'elrun's Gaze](/spells/jelrunsGaze.md) to charm individuals

## Reference Image

![](/.assets/img/jelrun.png)

## Music Choices

<iframe src="https://www.youtube.com/embed/2PNX8arJLoU" frameborder="0"></iframe>

<iframe src="https://www.youtube.com/embed/qA_kWCsFaSo" frameborder="0"></iframe>
