# Synrial, Archangel of Reaping

## Draft Notes

- Synrial is an [Archangel](/concepts/archangel.md) of [Keltok](keltok.md), God of Death
- Synrial's symbol is that of a sythe with a pole made of skulls
- Synrial uses a massive sythe able to cleave through all armours
- Angels of Synrial are tasked with reaping and collecting the souls of those who are destined for death
- Priests of Synrial can use [Synrial's Toll](/spells/synrialsToll.md) to protect individuals from harm

## Reference Image

![](/.assets/img/synrial.png)
