# Tormial, God of Peace

## Draft Notes

- Tormial usually takes the form of an old man in robes of modest quality.
- Tormial's Archangels are devoted to the maintenance of peace throughout the realms.
- Tormial's Archangels are:
  - [Lesdan](lesdan.md), Archangel of Peace and Prosperity

## Reference Image

![](/.assets/img/tormial.png)

## Music Choices

<iframe src="https://www.youtube.com/embed/OPlK5HwFxcw" frameborder="0"></iframe>
