# Bellari, Archangel of Restoration

## Draft Notes

- Bellari is an [Archangel](/concepts/archangel.md) of [Hera](hera.md), Goddess of Life
- Bellari's symbol is a heart-shaped gem with a crack in it
- Bellari wields no weapon, they never engage in combat
- Angels of Bellari disguise themselves and infiltrate society to provide healing to the needy
- Priests of Bellari can use [Aura of Restoration](/spells/auraOfRestoration.md) to heal many people in an area at once

## Reference Image

![](/.assets/img/bellari.png)
