# Rules

We use all standard D&D 5e rules, though we have made these following changes for our own games:

## Combat

- Instead of one movement and one attack per turn, **the player has two general actions** and can therefor attack twice by default. Some attacks and abilities, however, may hold a cost of two actions instead of one.
- **Duel-wielding** without the Two-Weapon Fighter feat takes a bonus action to do per attack, otherwise it doesn't cost anything and can be done every attack.
- Getting a **critical success on initiative rolls** allows the player one more action in their initial turn, whereas a **critical fail** allows them only one action instead. Bonus actions and reactions are not affected.
- When healing someone who's unconscious, you must spend 1 point of healing on the character if they are unstable.

## Racial Traits

### Warforged

- The **Warforged Resilience** rule has changed: You need to "sleep," you do suffer exhaustion, but magic still can't put you to sleep.
