# Halldiklif

[[TOC]]

## Draft Notes

### Population
- Approximately 600; mostly human, some other civilized races.
### Government
- Halldiklif is governed by an arcane sorcerer, a male human named Saki Rinto.
### Notable Places
- The House of Pythippo: A large half-timbered theatre, said to be built upon the ground where a sidhe noble was betrayed and murdered.
- Shrine of Jallak Maa'al: A cauldron lamp enshrining the flame of [Jallak Maa'al](/characters/jalaakmaaal.md), Goddess of Chaos, said to be sometimes visited by the goddess herself.
- Wone and Pervy's Quill: A scribery.
- The Gray Flagon: A heroic inn, said to be built atop an ancient dungeon.
- The Gypsy and Flask: A less reputable inn.
### A few NPCs
- Sawa Hiko: Female Human Druid, Neutral. Hiko has copper hair and brown eyes. She wears leather armor and wields a club and dagger.
- Kawa Emin: Female Human Scholar, Neutral. Emin is tall and thin, with messy auburn hair and brown eyes. She wears well-made clothing and a sling of vials and potions. Emin is amoral and dishonest.
- Tsugi Saeko: Female Human Alchemist, Good. Saeko has black hair and brown eyes, and large ears. She wears modest garments and carries a fine stiletto. Saeko seeks to atone for past sins.
- Katsu Yosun: Male Human Entertainer, Evil. Yosun is pleasant in appearance, with thick golden hair and light hazel eyes. He wears modest garments and an amulet of luminous crystal. Yosun compulsively plays with a gold ring.
- Tsaki Shiga: Male Human Alchemist, Good. Shiga is common in appearance, with silver hair and dark hazel eyes. He wears well-made clothing and a dragonscale cloak.

## Wone and Pervy's Quill
### Location
- In an adventurers district known for its many teleporation circles. The street outside is lined with street vendors hawking swords and daggers.
### Description
- The shop is a three-storey tower of living wood, with a tiled mosaic floor. A collection of ornate ink flasks and - quills rests upon a shelf.
### Shopkeeper
- The shopkeeper is a thin male human named Wone. He only accepts payment in platinum coins.
### Items
- Spell Scroll (Awaken) (rare, dmg 200)
- Spell Scroll (Beast Sense) (uncommon, dmg 200)
- Spell Scroll (Comprehend Languages) (common, dmg 200)
- Spell Scroll (Warding Bond) (uncommon, dmg 200)

## The Gray Flagon
### Location
- On Brick Street, in a market district of noisy inns and street performers. The street outside is lined with street vendors hawking their goods.
### Description
- The inn is a large stone-walled building, with carved wooden doors. Accomodations consist of several small rooms with beds and woolen mattresses.
### Innkeeper
- The innkeeper is a cultured male elf named Tare. He has an animal companion, a griffon named Arwenwel.
### Menu
- Roasted Shellfish and Apricot Pie, Glass of Mead (3 sp)
- Steamed Oxen and Dried Orange, Glass of Brandy (2 sp)
- Braised Duck and Dried Dragonfruit, Glass of Rum (2 sp)
- Baked Goose with Nutmeg and Hazelnut Bread, Glass of Whisky (4 sp)
- Boiled Crayfish and Bitter Cheese, Tankard of Perry (12 cp)
- Braised Pheasant with Mace and Pomegranate Pie, Glass of Cider (2 sp)
### Patrons
- Warder: Male Halfling Illusionist, Evil. Warder is rugged in appearance, with gray hair and green eyes. He wears modest garments and wields a quarterstaff and dagger. Warder suffers a mild allergy to horses.
- Umrat: Male Dwarf Assassin, Neutral. Umrat is short and stout, with white hair and hazel eyes. He wears leather armor and wields a poisoned long sword and dagger. Umrat seeks an opponent for a game of dice.
- Mose Boffin: Female Halfling Druid, Neutral. Mose is exceptionally beautiful, with tangled golden hair and hazel eyes. She wears leather armor and wields a spear and sling. Mose is searching for her lost sister.
- Finy: Male Human Fighter, Evil. Finy has braided blonde hair and blue eyes, and a distinctive scar on his leg. He wears banded mail and wields a mace and heavy crossbow. Finy seeks to fulfill an ancient prophecy of evil.
- Ennan: Male Elf Necromancer, Evil. Ennan has red hair and soft hazel eyes, and walks with a limp. He wears expensive clothing and wields a club. Ennan compulsively plays with a steel ring.
- Bilswe: Female Human Fighter, Neutral. Bilswe is exceptionally beautiful, with tangled black hair and blue eyes. She wears banded mail and wields a flail and heavy crossbow. Bilswe seeks opponents for a game of darts.
- Herard Urrok: Male Halfling Paladin, Good. Herard has a long face, with straight blonde hair and large hazel eyes. He wears banded mail and wields a bardiche. Herard is talking quietly with a group of lost pilgrims.
- Lana: Female Halfling Fighter, Good. Lana is slender, with gray hair and light green eyes. She wears splint mail and wields a short sword and heavy crossbow. Lana seeks to become a knight of the realm.
### Rumors
- A celestial archon dwells in the Cloister of the Cerulean Sky.
- Fryffi's Hold is beseiged by the armies of Illfang the Kobold Lord.
- An insane lich lurks in the ruins of Minas Hine.
- The illustrious sorceress Ampen is held imprisoned within the Delve of Bloody Death, frozen in time by an evil lich.
- Something has been delving a series of cysts beneath the town.
- Demaiosius the Alchemist has disappeared, along with his living dreams.
- A spectral dragon has been seen in the Toli Veldt.
- The Abbey of Angels exists in many different places and times at once.

## The Gypsy and Flask (More Info)
### Location
- In a warehouse district known for its black markets. The street outside is obstructed by stacks of crates and barrels.
### Description
- The inn is a single storey wooden building, with unusually low ceilings. Accomodations consist of several wooden cots in the cellar and woolen blankets near the hearth. Many of the tables in the common room are used for gambling.
### Innkeeper
- The innkeeper is an impartial female elf named Cardia. She seems to know every rogue and thief in town.
### Menu
- Wheat Porridge, Mug of Bitter (4 cp)
- Boiled Veal and Curd Cheese, Tankard of Beer (10 cp)
### Patrons
- Maere: Male Human Craftsman, Neutral. Maere has brown hair and hazel eyes. He wears sturdy clothing and riding boots. Maere is searching for the lost dominion of Briathone.
- Ilindon: Male Elf Scholar, Evil. Ilindon is short, with straight copper hair and gray eyes. He wears well-made clothing and a sling of vials and potions. Ilindon is fascinated by geometry.
- Lota: Female Halfling Mercenary, Good. Lota has red hair and brown eyes, and a distinctive scar on her face. She wears scale mail and wields a warhammer. Lota seeks opponents for a drinking contest.
- Shari: Male Dwarf Soldier, Good. Shari has a long face, with white hair and narrow amber eyes. He wears scale mail and wields a spear. Shari seeks to save his family from financial ruin.
- Golda Burrubb: Female Halfling Artist, Evil. Golda is fair in appearance, with messy brown hair and narrow gray eyes. She wears tailored clothing and a dragonscale cloak. Golda lost her reflection to a nymph.
I- rarl: Female Halfling Scofflaw, Neutral. Irarl is short and slender, with gray hair and gray eyes. She wears sturdy clothing and carries a yew staff. Irarl suffers an acute fear of clerics.
### Rumors
- An order of demonic cultists has stolen an ancient artifact from the lost city of Spreywood in the Babon Waste.
- Cuthhelm the harlot was once an adventurer but was maimed by trolls in the Lost Tomb of Nightmares.
- A vampire still lurks in the crypts beneath the ruins of Caer Anthil.
- The dragon Amminum has burned the Corwy Forest to ash and cinders.
- A vast swarm of spiders has been gathering in the Forest of Thorns.
- Cily has been searching the area near the Shrine of Dreams.
- A perpetual storm rages over the Dusk Jungle.

## Reference Image

![](/.assets/img/halldiklif.png)


## Music Choices

<iframe src="https://www.youtube.com/embed/HkIzMuvIhNQ" frameborder="0"></iframe>
